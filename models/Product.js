const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description : {
		type: String,
		required: [true, "description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	slots: {
		type: Number,
		required: [true, "Slots is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
			type: Date,
			default: new Date()
	},
	orders: [
		{	
			userId: {
				type: String,
				required: [true, "User Id is required"]
			}
		}
	]
})
module.exports = mongoose.model("Product", productSchema);