const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.ly122zp.mongodb.net/decode_ph?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}	
);

// Set notifications for database connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connections error"));
db.once("open", () => console.log("We're connected to the cloud database"))


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes)
app.use("/products", productRoutes)



// Will use the defined port number for the application whenever an environment variable is available OR will use port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
})