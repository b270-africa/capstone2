const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Invokes the checkEmailExists function from the controller to communicate with our database
router.post("/checkEmail", userController.checkEmailExists);

// Route for user registration
router.post("/register", userController.registerUser);

// Route for user authentication
router.post("/login", userController.loginUser);

// Route for creating order
router.post("/checkout", auth.verify, userController.checkout);

// Route for getting a specific user's details
router.get("/userDetails", auth.verify, userController.getProfile);

// Route for updating user access
router.put("/:userId/update", auth.verify, userController.updateUser);


module.exports = router;

