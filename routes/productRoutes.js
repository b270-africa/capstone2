const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require ("../auth");

// Route for add a new product
router.post("/", auth.verify, productController.addProduct);

router.get("/all", auth.verify, productController.getAllProducts);

// Route for retrieving all ACTIVE products
router.get("/active", productController.getAllActive);

// Route for retrieving a single product
router.get("/:productId", productController.getProduct);

// Route for updating a product
router.put("/:productId", auth.verify, productController.updateProduct);

// Route for archiving a product
router.put("/:productId/archive", auth.verify, productController.archiveProduct);


module.exports = router;
