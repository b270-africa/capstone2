const Product = require("../models/Product");
const auth = require("../auth")

// Add a new product

module.exports.addProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		
		let newProduct = new Product({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		});

		return newProduct.save()
		.then(product => {
		console.log(product);
		res.send(product);
		})
		.catch(error => {
		console.log(error);
		res.send(false);
		})
	} else {
		return res.send({message: "Access denied! Please contact your Administrator"});
	}

}

// Retrieve all products
/*
	Step:
	1. Retrieve all the courses from the database
*/
module.exports.getAllProducts = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		return Product.find({}).then(result => res.send(result));
	} else {
		return res.send(false);
	}
	
}

// Retrieve all ACTIVE product

module.exports.getAllActive = (req, res) => {

	return Product.find({isActive: true}).then(result => res.send(result));
}

// Retrieving a single product

module.exports.getProduct = (req, res) => {

	console.log(req.params.productId)

	return Product.findById(req.params.productId)
	.then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(error);
	})
}

// Update a product

module.exports.updateProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		let updateProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			// isActive: req.body.isActive
			slots: req.body.slots
		}

		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true})
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	}	else {
		return res.send({message: "Access denied! Please contact your Administrator"});
	}
}

// Archive a product
module.exports.archiveProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	if(userData.isAdmin) {
		let archiveProduct = {
			isActive: req.body.isActive
		}

		return Product.findByIdAndUpdate(req.params.productId, archiveProduct)
		.then(result => {
			return res.send({message: "Update Successful"})
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	}	else {
		return res.send({message: "Access denied! Please contact your Administrator"});
	}
}