const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if the email already exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend application based on the result of the "find" method
*/
module.exports.checkEmailExists = (req, res) => {
	return User.find({email: req.body.email}).then(result => {

		// The "find" method return a record if a match is found
		if(result.length > 0) {
			return res.send (true);

		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return res.send (false);
		}
	})
	.catch(error => res.send (error))
}

// User registration

module.exports.registerUser = (req, res) => {

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	// Saves the created object to our database
	return newUser.save().then(user => {
		console.log(user);
		res.send(true)
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
}

// User authentication

module.exports.loginUser = (req, res) => {

	return User.findOne({email: req.body.email}).then(result => {

		// User does not exist
		if(result == null) {

			return res.send({message: "Email or Password is incorrect"});

		// User exists
		} else {

			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect) {

				// Generates an access token by invoking the "createAccessToken" in the auth.js file
				return res.send({accessToken: auth.createAccessToken(result)});

			// Passwords do not match
			} else {
				return res.send({message: "Email or Password is incorrect"});
			}
		}
	})
};

module.exports.checkout = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(!userData.isAdmin) {
		// To retrieve the course name
		let productName = await Product.findById(req.body.productId)
		.then(result => result.name);


		let data = {

			// User ID and email will be retrieved from the payload/token
			userId: userData.id,
			email: userData.email,
			// Course ID will be retrieved from the request body
			productId: req.body.productId,
			productName: productName
		}
		console.log(data);


		// Add the course ID in the enrollments array of the user
		// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
		// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend

		let isUserUpdated = await User.findById(data.userId).then(user => {

			// Adds the courseId in the user's enrollments array
			user.orders.push({
				productId: data.productId,
				
			});

			// Saves the updated user information in the database
			return user.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isUserUpdated);


		// Add the user ID in the enrollees array of the course
		// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend

		let isProductUpdated = await Product.findById(data.productId)
		.then(product => {

			// Adds the userId in the course's enrollees array
			product.orders.push({
				userId: data.userId
			})
		
			// Minus the slots available by 1
			product.slots -= 1;

			// Saves the updated course information in the database
			return product.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isProductUpdated);


		// Condition that will check if the user and course documents have been updated

		// User enrollment successful
		if(isUserUpdated && isProductUpdated) {
			return res.send(true);

		// User enrollment failure
		} else {
			return res.send(false);
		}
	} else {
		return res.send(false);
	}

}

module.exports.getProfile = (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {

		result.password = "";

		return res.send(result);
	});

};

// Update user access

module.exports.updateUser = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		let updateUser = {
			isAdmin: req.body.isAdmin
		}

		return User.findByIdAndUpdate(req.params.userId, updateUser, {new:true})
		.then(result => {

			result.password = "";
			
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	}	else {
		return res.send(false);
	}
}
